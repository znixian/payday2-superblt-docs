
# Developer Console

The developer console is available via creating a text file to enable it.

### Enabling the Developer Console

Create a blank text file called `developer.txt` in the `mods` folder and then restart the game.
