# Delayed Calls

Delayed Calls are function calls which can be delayed by a set amount of time. They are delayed by a time value in seconds, if you want to delay a call until a specific event happens, or another function is ran, you may want to use [Hooks](hooks.md) instead.

## Creating a Delayed Call

```lua
 DelayedCalls:Add(id, time, func)
```

Adds a delayed call that will automatically be ran after the time has expired.  
`id` The unique identifier of this delayed call.  
`time` A time value, in seconds, that the function `func` should run after.  
`func` The function that should be ran if `time` passes.  

Example:

```lua
DelayedCalls:Add("DelayedCallsExample", 5, function()
  log("This will be called after 5 seconds.")
end)
```

Note that when you create a dlayed call with an id that already exists, it will override the existing one.

## Removing a Delayed Call

```lua
DelayedCalls:Remove(id)
```

Removes a delayed call with the identifier that matches `id`. If the delayed call does not exist, or has expired, this function does nothing.  
`id` The unique identifier of the delayed call to attempt to remove.  

Example:

```lua
DelayedCalls:Remove("DelayedCallsExample")
```

Delayed calls that have been executed are automatically removed so you only need to use this when you want to prevent the execution of a delayed call that hasn't been executed yet.
