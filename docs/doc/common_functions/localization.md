# Localization Functions

A set of functions which are available to the LocalizationManager to add customized localization strings.

## Directly adding translation strings

```lua
LocalizationManager:add_localized_strings(tbl)
```

Registers a table of keys localized strings.  
`tbl` A table of localized strings, where the key is the localization id, and the value is the localized string to fetch.  

Example:

```lua
-- Directly adding to the localized strings table
managers.localization:add_localized_strings({
  ["loc_example_test_string"] = "This is our localization test string!",
  ["loc_example_test_string2"] = "This is our localization test string!",
  ["loc_example_test_stringend"] = "This is the last string no comma is needed after this string!"
})

-- Automatically adding localized strings once the LocalizationManager has loaded
Hooks:Add("LocalizationManagerPostInit", "LocalizationManagerPostInit_LocExample", function(localization_manager)

  localization_manager:add_localized_strings({
    ["loc_example_test_string"] = "This is our localization test string!",
    ["loc_example_test_string2"] = "This is our localization test string!",
    ["loc_example_test_stringend"] = "This is the last string no comma is needed after this string!"
  })

end)
```

## Loading translation strings from a file

```lua
LocalizationManager:load_localization_file(file_path)
```

Loads a JSON formatted file and adds all keys and values to the localization table.  
`file_path` The file to be loaded, and registered in the localization manager.  

Example:

JSON

```json
{
  "loc_example_json_file" : "This is a localization string being loaded from JSON",
  "loc_example_json_file2" : "This is the last string no comma is needed after this string!"
}
```

Lua

```lua
managers.localization:load_localization_file("loc.json")
```

## Using translation strings

To use localization strings in your mod, you can request the text from the localization manager in the same way the game does.

```lua
local text = managers.localization:text("loc_example_test_string")
log(text)
--> "This is our localization test string!"
```
