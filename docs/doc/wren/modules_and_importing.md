# Modules and importing

See the Wren documentation for how imports work in general. However, the exact way that
the import strings are resolved to files is specific to SuperBLT.

For the given import statement:

```wren
import "module-path-goes-here"
```

... there's a few rules for which these imports are tested under. These are:

## Module-relative

Anything starting with ./ means relative to this module (Wren terminology for file).

The file `mods/mymod/wren/a/b/myfile.wren`:

```wren
import "./myotherfile.wren"
```

... will import `mods/mymod/wren/a/b/myotherfile.wren`.

## Mod-relative

Anything starting with .../ means relative to this mod.

The file `mods/mymod/wren/a/b/myfile.wren`:

```wren
import ".../myotherfile.wren"
```

... will import `mods/mymod/wren/myotherfile.wren`.

Notably, for files that aren't inside extra folders (such as `a/b` in the example above) this is the same as `./`

## SuperBLT/Basemod imports

Anything starting with base/ refers to files in the basemod or DLL.

The valid files are:

* `base/native` contains the [core native APIs](core_sblt_api.md), such as those for logging.
* `base/native/DB_001` contains the [asset database hook API](assets.md).
* `base/native/LuaInterface_001` contains the Wren side of the [Wren-Lua IO API](wren_lua_io.md).
* `base/native/Environment_001` contains the Wren version of Lua's ModPath.

(there are other files that start with `internal`, however they may change between SuperBLT versions
and mods should never use them)

## Other mod API imports

Anything starting with mods/ refers to published files in another mod - yet to figure out how this will work.

For now just ignore this, but do know some kind of mod-to-mod API system should arrive in the future.

## Meta and Random modules

The 'meta' and 'random' modules from Wren can be used by name, as per usual.

## Raw file loads

Anything starting with __raw_force_load/ refers to the rest of that string directly. The
rest of the string must in the form of `mods/path/to/wren/file`.

Please don't abuse this! It requires hard-coding the mod folder name, so if someone renames
your mod and you use this then the game will crash on startup as it fails to resolve the import.
