# Network Helper

A collection of helper Lua functions to allow mods to send networked data to other players who have the mod installed.
All data is sent as string data between clients, so any non-string data, such as numbers or Vector3's, must be converted into a string before transmission.

Note: The `NetworkHelper` was previously called `LuaNetworking` and is currently still accessible through this alias.

## Networking information

### Check for multiplayer session

```lua
NetworkHelper:IsMultiplayer()
```

Checks if the game is in a multiplayer state, and has an active multiplayer session.  
`returns` The active multiplayer session, or `false`.  

### Check if the local player is host

```lua
NetworkHelper:IsHost()
```

Checks if the local player is the host of the multiplayer game session.  
Functionally identical to `Network:is_server()`.  
`returns` `true` if a multiplayer session is running and the local player is the host of it, or `false`.  

### Check if the local player is client

```lua
NetworkHelper:IsClient()
```

Checks if the local player is a client of the multiplayer game session.  
Functionally identical to `Network:is_client()`.  
`returns` `true` if a multiplayer session is running and the local player is not the host of it, or `false`.  

### Get the local player id

```lua
NetworkHelper:LocalPeerID()
```

Returns the peer ID of the local player.  
`returns` Peer ID of the local player or `0` if no multiplayer session is active.  

### Get the name of a player

```lua
NetworkHelper:GetNameFromPeerID(id)
```

Returns the name of the player associated with the specified peer ID.  
`id` Peer ID of the player to get the name from.  
`returns` Name of the player with peer ID `id`, or `"No Name"` if the player could not be found.  

### Get the list of players

```lua
NetworkHelper:GetPeers()
```

Returns an accessor for the session peers table.  
`returns` Table of all players in the current multiplayer session.  

### Get the number of players

```lua
NetworkHelper:GetNumberOfPeers()
```

Returns the number of players in the multiplayer session.  
`returns` Number of players in the current session.  

## Sending data

### Sending to everyone

```lua
NetworkHelper:SendToPeers(id, data)
```

Sends networked data with a message id to all connected players.  
`id` Unique name of the data to send.  
`data` Data to send.  

Example:

```lua
NetworkHelper:SendToPeers("GoonModCustomLaserColour", "1.0000000,0.0000000,1.0000000")
```

### Sending to a single player

```lua
NetworkHelper:SendToPeer(peer, id, data)
```

Sends networked data with a message id to a specific player.  
`peer` Peer ID of the player to send the data to.  
`id` Unique name of the data to send.  
`data` Data to send.  

Example:

```lua
local message_text = "This is a private message to the player with Peer ID 1."
NetworkHelper:SendToPeer(1, "PrivateMessage", message_text)
```

### Sending to everyone with exceptions

```lua
NetworkHelper:SendToPeersExcept(peer, id, data)
```

Sends networked data with a message id to all connected players except specific ones.  
`peer` Peer ID or table of peer IDs of the player(s) to exclude.  
`id` Unique name of the data to send.  
`data` Data to send.  

Example:

```lua
local message_text = "This message will be send to everybody who does not have a Peer ID of 4."
NetworkHelper:SendToPeersExcept(4, "PrivateMessage", message_text)

local exclude = { 1, 2 }
local message_text = "This message will be send to everybody who's peer ID does not appear in the exlude table"
NetworkHelper:SendToPeersExcept(exclude, "PrivateMessage", message_text)
```

## Receiving data

### The NetworkReceivedData hook

A specific hook is called when a Lua-network event is received from another player. In order to make use of the data received from other players, your mod should add to the hook in order to process the data received and call the appropriate functions required.

For more information on listening to hooks, see the main [Hooks](common_functions/hooks.md) page.

The name of the hook that is called everytime any data is received via NetworkHelper from another player.  
`sender` The peer ID of the player who sent the networked data.  
`id` The unique identifier of the data sent.  
`data` The data sent to us, as a string. It may have to be converted to another data type in order for us to make use of it.  

```lua
local private_message_id = "PrivateMessage"
Hooks:Add("NetworkReceivedData", "NetworkReceivedData_PMs", function(sender, id, data)

  if id == private_message_id then

    local name = NetNetworkHelper:GetNameFromPeerID(sender)
    log("Received Private Message from: " .. name)
    log("Message: " .. data)

  end

end)
```

As we can see in our example above, once subscribed to our hook we need to check the ID of the data we receive to make sure that the data is relevant to our mod.
If the data we receive is not relevant, then we should not process or call anything, as another mod is likely handling it instead.

### Receive hooks

This is new functionality that lets you directly hook a function to a specific message ID so you don't have to set up a `NetworkReceivedData` hook and check the IDs manually.

#### Adding a hook

```lua
NetworkHelper:AddReceiveHook(message_id, hook_id, func)
```

Registers a function to be called when network data with a specific message id is received.  
`message_id`The message id to hook to.  
`hook_id` A unique name for this hook.  
`func` Function to be called when network data for that specific message id is received.  

Example:

```lua
NetworkHelper:AddReceiveHook("PrivateMessage", "PrivateMessageReceiveHook", function(data, sender)

    local name = NetworkHelper:GetNameFromPeerID(sender)
    log("Received Private Message from: " .. name)
    log("Message: " .. data)

end)
```

The example above shows a comparison between the old `NetworkReceivedData` hook and the new newtork receive hooks.
As you can see, with receive hooks we can make sure our function is only called when a message with that specific ID is received without having to manually check the ID.

#### Removing a hook

```lua
NetworkHelper:RemoveReceiveHook(hook_id, message_id)
```

Removes a receive hook.  
`hook_id` The unique name of the hook to remove.  
`message_id` The message id to remove the hook from, optional, if not specified removes all matching hooks.  

## Extensions

Various extension and helper functions for converting data types that may not normally be convertible into a string, into a string and back into their original type.

### Converting colors

#### Color to string

```lua
NetworkHelper:ColourToString(col)
```

Converts a colour into a formatted string, which is able to be sent through the Networking functions.  
`col` The colour to be converted into a formatted string.  
`returns` A formatted string containing the colour data from `col`.  

```lua
local color = Color.red
local col_str = NetworkHelper:ColourToString(color)
NetworkHelper:SendToPeers("NetworkColourStringTest", col_str)
```

#### String to color

```lua
NetworkHelper:StringToColour(str)
```

Converts a formatted string into a colour. Should be used for turning a networking colour sent as a string back to its original object.  
`str` The formatted string to attempt to convert back into a Color object.  
`returns` A Color object containing the original colour.  

```lua
  local col_str = "r:1.0000|g:0.2125|b:0.4568|a:1.0000"
  local color = NetworkHelper:StringToColor(col_str)
```

### Converting vectors

#### Vector to string

```lua
NetworkHelper:Vector3ToString(v)
```

Converts a Vector3 to a formatted string.  
`vec` The Vector3 to convert to a string.  
`returns` A formatted string with the information of the input `vec`.  

```lua
local vec = Vector3(50, 0, 10)
NetworkHelper:Vector3ToString(vec)
```

#### String to vector

```lua
NetworkHelper:StringToVector3(string)
```

Converts the formatted string `str` into a usable Vector3.  
`str` The formatted string to convert into a Vector3.  
`returns` A Vector3.  

```lua
local vec_str = "50.000000,25.456841,00.000000"
NetworkHelper:StringToVector3(vec_str)
```
